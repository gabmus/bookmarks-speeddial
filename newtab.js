"use strict";
const bms = browser.bookmarks;
const mainTag = document.getElementById('main');
const db = browser.storage.sync;
const body = document.getElementsByTagName('body')[0];
let getIconsFromWebsites = false;
const SD_KEY = 'speed-dial-folders';
const KNOWN_WEBSITES = {};
let KW_URLS = [];
const contextMenu = document.getElementById('contextMenu');
const innerContextMenu = document.getElementById('innerMenu');
const contextActionOpenNewTab = document.getElementById('contextActionOpenNewTab');
const contextActionEditBookmark = document.getElementById('contextActionEditBookmark');
const contextActionRemoveBookmark = document.getElementById('contextActionRemoveBookmark');
const contextActionCopyLink = document.getElementById('contextActionCopyLink');
let contextItemLink;
let contextItemBookmarkId;
contextActionOpenNewTab.onclick = () => {
    if (!contextItemLink) {
        return;
    }
    window.open(contextItemLink, '_blank').focus();
};
contextActionRemoveBookmark.onclick = () => {
    if (!contextItemBookmarkId) {
        return;
    }
    const scroll = document.documentElement.scrollTop;
    bms.remove(contextItemBookmarkId).then(() => {
        populateAll().then(() => {
            document.documentElement.scrollTop = scroll;
        });
    });
};
const editBookmarkModal = document.getElementById('editBookmarkModal');
const editBookmarkTitleInput = document.getElementById('editBookmarkTitleInput');
const editBookmarkUrlInput = document.getElementById('editBookmarkUrlInput');
const editBookmarkCancelBtn = document.getElementById('editBookmarkCancelBtn');
const editBookmarkSaveBtn = document.getElementById('editBookmarkSaveBtn');
contextActionEditBookmark.onclick = () => {
    if (!contextItemBookmarkId) {
        return;
    }
    bms.get(contextItemBookmarkId).then((res) => {
        const bm = res[0];
        editBookmarkTitleInput.value = bm.title;
        editBookmarkUrlInput.value = bm.url ?? '';
        editBookmarkModal.classList.remove('hidden');
    });
};
editBookmarkCancelBtn.onclick = () => {
    editBookmarkModal.classList.add('hidden');
};
editBookmarkSaveBtn.onclick = () => {
    if (contextItemBookmarkId) {
        const scroll = document.documentElement.scrollTop;
        bms.update(contextItemBookmarkId, {
            title: editBookmarkTitleInput.value,
            url: editBookmarkUrlInput.value,
        }).then(() => {
            populateAll().then(() => {
                document.documentElement.scrollTop = scroll;
                editBookmarkModal.classList.add('hidden');
            });
        });
    }
};
contextActionCopyLink.onclick = () => {
    if (!contextItemLink) {
        return;
    }
    navigator.clipboard.writeText(contextItemLink);
};
contextMenu.onclick = () => {
    contextMenu.classList.add('hidden');
};
const COLORS = [
    '#1c71d8',
    '#2ec27e',
    '#f5c211',
    '#e66100',
    '#c01c28',
    '#813d9c',
    '#865e3c',
];
function getCard(name) {
    return `./cards/${name}.svg`;
}
async function loadKnownWebsites() {
    const res = await fetch('./known_websites.csv');
    if (!res.ok) {
        console.error('Unable to load known_websites.csv');
        return;
    }
    const kw = [
        (await db.get({ customCardDefs: '' })).customCardDefs.trim(),
        (await res.text()).trim(),
    ]
        .join('\n')
        .trim();
    kw.split('\n')
        .filter((row) => row.trim() !== '' && row.trim().includes(';'))
        .map((row) => {
        const split = row.split(';');
        if (!KNOWN_WEBSITES[split[0]]) {
            KNOWN_WEBSITES[split[0]] = split[1];
        }
    });
    KW_URLS = Object.keys(KNOWN_WEBSITES);
}
function randomColor(s) {
    let n = 0;
    for (let i = 0; i < s.length; i++) {
        n += s.charCodeAt(i);
    }
    return COLORS[n % COLORS.length];
}
function makeGenericSvgIcon(_host) {
    const host = _host.replace('www.', '');
    const color = randomColor(host);
    const letter = host[0].toUpperCase();
    return ('data:image/svg+xml;base64,' +
        btoa(`
<svg width="256" height="128" version="1.1" viewBox="0 0 67.733 33.867" xmlns="http://www.w3.org/2000/svg">
 <defs>
  <linearGradient id="a" x1="-187.51" x2="-187.51" y1="-136.01" y2="-68.376" gradientTransform="matrix(1 0 0 .5 222.27 68.05)" gradientUnits="userSpaceOnUse">
   <stop stop-color="#ffffff" stop-opacity=".4" offset="0"/>
   <stop stop-color="#ffffff" stop-opacity="0" offset="1"/>
  </linearGradient>
 </defs>
 <rect x="-1.7012e-6" y="-8.5059e-7" width="67.733" height="33.867" fill="${color}" stroke-linecap="round" stroke-width="3.4791" style="paint-order:markers stroke fill"/>
 <text x="33.849792" y="28.738415" fill="#ffffff" font-family="sans-serif" font-size="33.729px" letter-spacing="0px" stroke-width=".26458" word-spacing="0px" style="line-height:1.25" xml:space="preserve"><tspan x="33.849792" y="28.738415" fill="#ffffff" font-family="'sans-serif'" font-weight="bold" stroke-width=".26458" text-align="center" text-anchor="middle" style="font-family: sans-serif;">${letter}</tspan></text>
 <rect x="-1.7012e-6" y="-4.253e-7" width="67.733" height="33.867" fill="url(#a)" stroke-linecap="square" stroke-width="1.3229" style="paint-order:markers stroke fill"/>
</svg>
    `
            .trim()
            .replace('\n', '')));
}
function createTile(url, title, bookmarkId) {
    const item = document.createElement('a');
    item.dataset.bookmarkId = bookmarkId;
    item.classList.add('tile');
    item.draggable = true;
    item.ondragstart = (ev) => {
        const target = ev.target;
        target.classList.add('dragging');
    };
    const host = new URL(url).host;
    const match = KW_URLS.find((kw) => host.includes(kw) || url.includes(kw));
    if (match) {
        const cardImg = getCard(KNOWN_WEBSITES[match]);
        item.style.backgroundImage = `url('${cardImg}')`;
    }
    else {
        const icon = document.createElement('img');
        icon.candidateSrcs = getIconsFromWebsites
            ? ['apple-touch-icon.png', 'favicon.png', 'favicon.ico']
            : [];
        icon.onerror = () => {
            icon.candidateSrcs.shift();
            if (icon.candidateSrcs.length > 0) {
                icon.src = `https://${host}/${icon.candidateSrcs[0]}`;
            }
            else {
                item.removeChild(icon);
                item.style.backgroundImage = `url('${makeGenericSvgIcon(host)}')`;
            }
        };
        if (icon.candidateSrcs.length > 0) {
            icon.src = `https://${host}/${icon.candidateSrcs[0]}`;
            item.appendChild(icon);
        }
        else {
            item.style.backgroundImage = `url('${makeGenericSvgIcon(host)}')`;
        }
    }
    const span = document.createElement('span');
    span.appendChild(document.createTextNode(title));
    item.appendChild(span);
    item.href = url;
    item.oncontextmenu = (ev) => {
        innerContextMenu.style.left = `${ev.x}px`;
        innerContextMenu.style.top = `${ev.y}px`;
        contextMenu.classList.remove('hidden');
        contextItemLink = url;
        contextItemBookmarkId = bookmarkId;
        return false;
    };
    return item;
}
function createBookmarksSection(title, items) {
    const details = document.createElement('details');
    const summary = document.createElement('summary');
    summary.textContent = title;
    details.appendChild(summary);
    details.open = true;
    const grid = document.createElement('div');
    grid.classList.add('grid');
    grid.ondragover = (ev) => {
        ev.preventDefault();
    };
    grid.ondragend = async (ev) => {
        const allChildren = Array.from(document.querySelectorAll('.tile'));
        const dragging = document.querySelectorAll('.dragging')[0];
        const closest = allChildren.reduce((oldChild, newChild) => {
            if (!oldChild) {
                return newChild;
            }
            const newBb = newChild.getBoundingClientRect();
            const oldBb = oldChild.getBoundingClientRect();
            const newCenter = [
                newBb.left + newBb.width / 2,
                newBb.top + newBb.height / 2,
            ];
            const oldCenter = [
                oldBb.left + oldBb.width / 2,
                oldBb.top + oldBb.height / 2,
            ];
            const newDistance = Math.sqrt((ev.clientX - newCenter[0]) ** 2 +
                (ev.clientY - newCenter[1]) ** 2);
            const oldDistance = Math.sqrt((ev.clientX - oldCenter[0]) ** 2 +
                (ev.clientY - oldCenter[1]) ** 2);
            return newDistance < oldDistance ? newChild : oldChild;
        }, undefined);
        if (!closest) {
            console.error('no closest found?');
            return;
        }
        const closestBb = closest.getBoundingClientRect();
        const left = ev.clientX < closestBb.left + closestBb.width / 2;
        dragging.classList.remove('dragging');
        if (closest.dataset?.bookmarkId === dragging.dataset?.bookmarkId) {
            return;
        }
        if (!dragging.dataset.bookmarkId) {
            console.error('tile has no bookmarkId');
            return;
        }
        if (!closest.dataset.bookmarkId) {
            console.error('closest tile has no bookmarkId');
            return;
        }
        const closestBm = (await bms.get(closest.dataset.bookmarkId))[0];
        if (closestBm.index === null || closestBm.index === undefined) {
            console.error('closest tile associated bookmark has no index');
            return;
        }
        if (!closest.parentElement) {
            console.error('closest tile has no parent element?');
            return;
        }
        const draggingBm = (await bms.get(dragging.dataset.bookmarkId))[0];
        if (draggingBm?.index === null || draggingBm?.index === undefined) {
            console.error('dragging tile associated bookmark has no index');
            return;
        }
        let targetIndex = closestBm.index;
        if (draggingBm.parentId === closestBm.parentId) {
            if (draggingBm.index < closestBm.index) {
                if (left) {
                    targetIndex -= 1;
                }
            }
            else if (!left) {
                targetIndex += 1;
            }
        }
        else if (!left) {
            targetIndex += 1;
        }
        await bms.move(dragging.dataset.bookmarkId, {
            parentId: closestBm.parentId,
            index: targetIndex,
        });
        const scroll = document.documentElement.scrollTop;
        await populateAll();
        document.documentElement.scrollTop = scroll;
    };
    for (const item of items) {
        grid.appendChild(item);
    }
    details.appendChild(grid);
    return details;
}
async function loadFavoritesFolders(folders) {
    if (folders.length <= 0) {
        return;
    }
    const out = await Promise.all(folders.map(async ({ title, id }) => {
        const bmRes = await bms.getChildren(id);
        const items = bmRes
            .filter((bm) => bm.type === 'bookmark' && !!bm.url)
            .map((bm) => {
            return createTile(bm.url, bm.title, bm.id);
        });
        return { title, items };
    }));
    for (const { title, items } of out) {
        mainTag.appendChild(createBookmarksSection(title, items));
    }
}
function populateSettings(folders) {
    const list = document.getElementById('speed-dial-folders');
    list.innerHTML = '';
    folders.map((folder) => {
        const fi = document.createElement('li');
        fi.draggable = true;
        fi.dataset.folderName = folder;
        fi.ondragstart = () => {
            list.classList.add('dragging');
            fi.classList.add('folder-item-dragging');
        };
        fi.ondragend = () => {
            list.classList.remove('dragging');
            for (const c of Array.from(list.children)) {
                c.classList.remove('folder-item-dragging');
            }
        };
        fi.ondragenter = (e) => {
            e.preventDefault();
        };
        fi.ondragover = (e) => {
            e.preventDefault();
            fi.classList.add('dragover');
        };
        fi.ondragleave = () => {
            fi.classList.remove('dragover');
        };
        fi.ondrop = () => {
            const target = fi;
            const source = list.getElementsByClassName('folder-item-dragging')[0];
            if (!source ||
                target.dataset.folderName === source.dataset.folderName) {
                return;
            }
            db.get({ [SD_KEY]: [] }).then((res) => {
                const newFolders = res[SD_KEY].filter((folder) => folder !== source.dataset.folderName);
                newFolders.splice(newFolders.indexOf(target.dataset.folderName) + 1, 0, source.dataset.folderName);
                db.set({ [SD_KEY]: newFolders }).then((_) => {
                    populateAll();
                });
            });
        };
        const span = document.createElement('span');
        span.appendChild(document.createTextNode(folder));
        fi.appendChild(span);
        const delBtn = document.createElement('button');
        delBtn.appendChild(document.createTextNode('×'));
        delBtn.onclick = () => {
            db.get({ [SD_KEY]: [] }).then((res) => {
                const newFolders = res[SD_KEY].filter((x) => x !== folder);
                db.set({ [SD_KEY]: newFolders }).then((_) => {
                    populateAll();
                });
            });
        };
        fi.appendChild(delBtn);
        list.appendChild(fi);
    });
    db.get({ customCardDefs: '' }).then((res) => {
        const defs = res.customCardDefs;
        const textbox = document.getElementById('customCardDefsTextbox');
        textbox.value = defs;
        textbox.readOnly = false;
    });
}
async function populateAll() {
    const res = await db.get({
        [SD_KEY]: [],
        getIconsFromWebsites: false,
    });
    getIconsFromWebsites = res.getIconsFromWebsites;
    mainTag.innerHTML = '';
    let folders = [];
    if (res[SD_KEY].length <= 0) {
        db.set({ [SD_KEY]: ['Speed Dial'] });
        folders = ['Speed Dial'];
    }
    else {
        folders = res[SD_KEY];
    }
    populateSettings(folders);
    const favFolders = [];
    for (const folder of folders) {
        const res = await bms.search({ title: folder });
        res.forEach((f) => favFolders.push({ title: f.title, id: f.id }));
    }
    await loadFavoritesFolders(favFolders);
}
function toggleSettings() {
    const settings = document.getElementById('settings');
    if (settings.className.includes('open')) {
        settings.classList.remove('open');
    }
    else {
        settings.classList.add('open');
    }
}
async function main() {
    await loadKnownWebsites();
    await populateAll();
    document.getElementById('settings-toggle').onclick = toggleSettings;
    document.getElementById('new-folder-btn').onclick = () => {
        const newFolderInput = document.getElementById('new-folder-input');
        const newFolder = newFolderInput.value.trim();
        newFolderInput.value = '';
        if (!newFolder) {
            return;
        }
        db.get({ 'speed-dial-folders': [] }).then((res) => {
            db.set({ 'speed-dial-folders': [...res[SD_KEY], newFolder] }).then((_) => {
                populateAll();
            });
        });
    };
    function initSetting(setFunc, inputId, onChange, resetBtnId, defaultVal) {
        const input = document.getElementById(inputId);
        input.onchange = onChange;
        if (resetBtnId && defaultVal) {
            const resetBtn = document.getElementById(resetBtnId);
            resetBtn.onclick = () => onChange({ target: { value: defaultVal } });
        }
        setFunc();
    }
    function setBgColor() {
        db.get({ 'bg-color': '#21222c' }).then((res) => {
            const target = res['bg-color'];
            body.style.backgroundColor = res['bg-color'];
            document.getElementById('bgColorInput').value = target;
        });
    }
    function onBgColorInputChange(ev) {
        db.set({ 'bg-color': ev.target.value });
        setBgColor();
    }
    initSetting(setBgColor, 'bgColorInput', onBgColorInputChange, 'resetDefaultBgColorBtn', '#21222c');
    function setFgColor() {
        db.get({ 'fg-color': '#ffffff' }).then((res) => {
            const target = res['fg-color'];
            body.style.color = target;
            document.getElementById('fgColorInput').value = target;
        });
    }
    function onFgColorInputChange(ev) {
        db.set({ 'fg-color': ev.target.value });
        setFgColor();
    }
    initSetting(setFgColor, 'fgColorInput', onFgColorInputChange, 'resetDefaultFgColorBtn', '#ffffff');
    function setBgImage() {
        let target = localStorage.getItem('bg-image');
        if (target === null) {
            ;
            body.style.backgroundImage = null;
        }
        else {
            target = target.replace(/(\r\n|\n|\r)/gm, '');
            body.style.backgroundImage = `url("${target}")`;
            body.style.backgroundSize = 'cover';
            body.style.backgroundRepeat = 'norepeat';
            body.style.backgroundPosition = 'center';
        }
    }
    function onBgImageChanged(ev) {
        if (!ev.target.files) {
            localStorage.setItem('bg-image', null);
            db.set({ 'bg-image': null });
            setBgImage();
            return;
        }
        const reader = new FileReader();
        reader.readAsDataURL(ev.target.files[0]);
        reader.onload = () => {
            localStorage.setItem('bg-image', reader.result);
            setBgImage();
        };
    }
    initSetting(setBgImage, 'bgImageInput', onBgImageChanged, 'resetDefaultBgImageBtn', null);
    function setFontFamily() {
        db.get({ 'font-family': 'sans-serif' }).then((res) => {
            const target = res['font-family'];
            body.style.fontFamily = target;
            document.getElementById('fontFamilyInput').value = target;
        });
    }
    function onFontFamilyChanged(ev) {
        db.set({ 'font-family': ev.target.value });
        setFontFamily();
    }
    initSetting(setFontFamily, 'fontFamilyInput', onFontFamilyChanged, 'resetFontFamilyBtn', 'sans-serif');
    function setFontSize() {
        db.get({ 'font-size': '1.2em' }).then((res) => {
            const target = res['font-size'];
            mainTag.style.fontSize = target;
            document.getElementById('fontSizeInput').value = target;
        });
    }
    function onFontSizeChanged(ev) {
        db.set({ 'font-size': ev.target.value });
        setFontSize();
    }
    initSetting(setFontSize, 'fontSizeInput', onFontSizeChanged, 'resetFontSizeBtn', '1.2em');
    function setGetIconsFromWebsites() {
        db.get({ getIconsFromWebsites: false }).then((res) => {
            const target = res.getIconsFromWebsites;
            document.getElementById('getIconsFromWebsites').checked = target;
            getIconsFromWebsites = target;
        });
    }
    function onGetIconsFromWebsitesChanged(ev) {
        db.set({ getIconsFromWebsites: ev.target.checked });
        setGetIconsFromWebsites();
    }
    initSetting(setGetIconsFromWebsites, 'getIconsFromWebsites', onGetIconsFromWebsitesChanged);
    function setCustomCardDefs() { }
    function onCustomCardDefsChanged(ev) {
        db.set({ customCardDefs: ev.target.value.trim() });
    }
    initSetting(setCustomCardDefs, 'customCardDefsTextbox', onCustomCardDefsChanged);
}
document.addEventListener('DOMContentLoaded', main);
